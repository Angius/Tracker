<?php
require ('includes/header.php');
?>

    <div class="ui centered stackable grid main-content">

        <div class="row"></div>

        <div class="relaxed row">

            <div class="eight wide column">
                <?php
                require ('db.php');
                require('includes/get-tickets.php');
                require ('includes/get-users.php');
                require ('includes/get-tags.php');
                require ('includes/get-categories.php');

                require ('includes/truncate.php');

                if (!empty($_GET && $_GET['id'] != "")) {

                    $tickets = getTicketsByTag($_GET['id']);
                    $tag = getSingleTag($_GET['id']);

                    ?>
                    <div class="ui raised segment">
                        <h2><?= $tag['tag_name'] ?></h2>
                    </div>
                    <?php

                    foreach ($tickets as $ticket) {

                        $poster = getSingleUser($ticket['ticket_author']);
                        $developer = getSingleUser($ticket['ticket_developer']);
                        $tags = getTagsByTicket($_GET['id']);

                        if (empty($developer)) {
                            $developer['user_id'] = "null";
                            $developer['user_name'] = "<i>NONE</i>";
                            $developer['user_avatar'] = "http://via.placeholder.com/260/ff8800/222222.png?text=NO+AVATAR";
                        }

                        // Check score
                        $colour = "";
                        if ($ticket['ticket_score'] < 0)
                            $colour = " red";
                        else if ($ticket['ticket_score'] > 0)
                            $colour = " green";
                        ?>

                        <div class="ui raised segments">
                            <div class="ui segment">
                                <a class="black-link" href="tickets.php?id=<?= $ticket['ticket_id'] ?>">
                                    <h3>
                                        <div class="ui <?= $colour ?> horizontal circular label">
                                            <i class="heart icon"></i>
                                            <?= $ticket['ticket_score'] ?>
                                        </div>
                                        <?= $ticket['ticket_name'] ?>
                                    </h3>
                                </a>
                            </div>
                            <div class="ui segment">
                                <?= truncate($ticket['ticket_body'], 255) ?>
                            </div>
                        </div>

                        <?php
                    }

                } else {

                    ?>

                    <div class="ui raised segment">
                        <h2>Tags</h2>
                    </div>

                    <div class="ui raised segments">

                        <?php

                        $tags = getAllTags();

                        foreach ($tags as $tag) {
                            ?>

                            <div class="ui segment">
                                <div class="ui grid">
                                    <div class="one wide column">
                                        <div class="ui label"><?=$tag['tag_id']?></div>
                                    </div>
                                    <div class="fifteen wide column">
                                        <h2 class="left floated header">
                                            <a href="?id=<?=$tag['tag_id']?>" class="black-link">
                                                <?=$tag['tag_name']?>
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }

                        ?>

                    </div>

                    <?php
                }
                ?>

            </div>

        </div>



        <div class="row"></div>

    </div>

<?php
require  ('includes/footer.php');
?>
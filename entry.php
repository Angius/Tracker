<!DOCTYPE html>
<html lang="en" class="bg-color">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Tracker</title>

    <link rel="stylesheet" href="css/registration-login.css">
    <link rel="stylesheet" href="semantic/dist/semantic.min.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="semantic/dist/semantic.min.js"></script>
    <script src="semantic/dist/components/tab.min.js"></script>
</head>

<body>

<a href="../" class="ui circular icon large button home-btn">
    <i class="home icon"></i>
</a>

<div class="ui middle aligned centered full-screen stackable grid" id="main-grid">

    <div class="logo">
        <img src="media/logo.svg">
        <h1>Trackr</h1>
    </div>

    <div class="row">
        <div class="four wide column">

            <div class="ui raised segment">

                <div class="ui secondary tabular menu">
                    <div class="active item" data-tab="tab-login">Log in</div>
                    <div class="item" data-tab="tab-register">Register</div>
                </div>

                <!-- Login tab -->
                <div class="ui active transition slide in tab" data-tab="tab-login">

                    <form class="ui form" id="login-form" method="post" action="includes/register-login.php">

                        <div class="ui error message"></div>

                        <div class="ui left icon input fluid field">
                            <input name="lemail" type="text" placeholder="E-mail">
                            <i class="mail icon"></i>
                        </div>

                        <div class="ui left icon action input fluid field">
                            <input name="lpass" type="password" placeholder="Password" id="lpass">
                            <i class="lock icon"></i>
                            <button class="ui icon button" type="button" data-content="Show/hide password" id="showhide" onclick="toggle_password('lpass')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>

                        <div class="field">
                            <div class="ui checkbox">
                                <input tabindex="0" class="hidden" type="checkbox">
                                <label>Remember me</label>
                            </div>
                        </div>

                        <input type="hidden" name="type" value="login">

                        <button class="ui fluid button" type="submit">Submit</button>
                    </form>

                </div>

                <!-- Registration tab -->
                <div class="ui transition slide in tab" data-tab="tab-register">
                    <form class="ui form" id="register-form" method="post" action="includes/register-login.php">

                        <div class="ui error message"></div>

                        <div class="ui left icon input fluid field">
                            <input name="login" type="text" placeholder="Login">
                            <i class="user icon"></i>
                        </div>

                        <div class="ui left icon input fluid field">
                            <input name="email" type="text" placeholder="E-mail">
                            <i class="mail icon"></i>
                        </div>
                        <div class="ui left icon input fluid field">
                            <input name="email2" type="text" placeholder="Confirm e-mail">
                            <i class="mail icon"></i>
                        </div>

                        <div class="ui left icon action input fluid field">
                            <input name="pass" type="password" placeholder="Password" id="pass">
                            <i class="lock icon"></i>
                            <button class="ui icon button" type="button" data-content="Generate random password" onclick='insertPassword(6, 2, 4, "pass")' >
                                <i class="random icon"></i>
                            </button>
                            <button class="ui icon button" type="button" data-content="Show/hide password" id="showhide" onclick="toggle_password('pass')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>
                        <div class="ui left icon action input fluid field">
                            <input name="pass2" type="password" placeholder="Confirm password" id="pass2">
                            <i class="lock icon"></i>
                            <button class="ui icon button" type="button" data-content="Show/hide password" id="showhide" onclick="toggle_password('pass2')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>

                        <div class="center field">
                            <div class="g-recaptcha" data-sitekey="6LdDFz4UAAAAAFhxj6JgCz1YvhNhLq4ZGYF89BE-"></div>
                        </div>

                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="terms">
                                <label>I agree to <a onclick="$('.ui.longer.modal').modal('show');">terms and conditions</a> </label>
                            </div>
                        </div>

                        <input type="hidden" name="type" value="register">

                        <button class="ui fluid button" type="submit">Submit</button>
                    </form>

                    <div class="ui longer modal">
                        <div class="header">Terms and Conditions</div>
                        <div class="scrolling content">
                            <?php require ('includes/terms.php') ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script>$('.tabular.menu .item').tab();</script>

<script src="js/generatePassword.js"></script>
<script src="js/showHidePassword.js"></script>

<script src="js/registration-login-validation.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

</body>

</html>

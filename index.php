<?php
require ('includes/header.php');
?>

<div class="ui centered stackable grid main-content">

    <div class="row"></div>

    <div class="relaxed row">

        <div class="three wide column"></div>

        <div class="eight wide column">
            <div class="ui raised segment">
                <h2>News</h2>
            </div>

            <?php
            require ('db.php');
            require ('includes/get-news.php');
            require ('includes/count-votes.php');
            require ('includes/truncate.php');

            $news = getAllNews();

            foreach (array_slice($news, 0, 5) as $n) {
                ?>
                <div class="ui raised segments">
                    <div class="ui segment">
                        <a class="black-link" href="news.php?id=<?=$n['news_id']?>">
                            <h3>
                                <div class="ui horizontal circular label">
                                    <?=date("d.m.Y",strtotime($n['news_date']))?>
                                </div>
                                <?=$n['news_title']?>
                            </h3>
                        </a>
                    </div>
                    <div class="ui segment">
                        <?=truncate($n['news_body'],$n['news_truncate'])?>
                        <a href="news.php?id=<?=$n['news_id']?>"> Read more</a>
                    </div>
                </div>
                <?php
            }
            ?>

            <a href="news.php" class="ui fluid animated simple raised segment button" tabindex="0">
                <div class="visible content">Older news...</div>
                <div class="hidden content">
                    <i class="right arrow icon"></i>
                </div>
            </a>

        </div>

        <div class="one wide column"></div>

        <div class="four wide column">
            <div class="ui raised segment">
                <h2>Recent issues</h2>
            </div>

            <?php
                require('includes/get-tickets.php');

                $tickets = getAllTickets();

                foreach (array_slice($tickets, 0, 5) as $ticket) {

                    // Check score
                    $colour = "";
                    if (countVotes($ticket['ticket_id']) < 0)
                        $colour = " red";
                    else if (countVotes($ticket['ticket_id']) > 0)
                        $colour = " green";
            ?>
                    <div class="ui raised segments">
                        <div class="ui segment">
                            <a class="black-link" href="tickets.php?id=<?=$ticket['ticket_id']?>">
                                <h3>
                                    <div class="ui <?=$colour?> horizontal circular label">
                                        <i class="heart icon"></i>
                                        <?=countVotes($ticket['ticket_id']);?>
                                    </div>
                                    <?=$ticket['ticket_name']?>
                                </h3>
                            </a>
                        </div>
                        <div class="ui segment">
                            <?=truncate($ticket['ticket_body'],255)?>
                        </div>
                    </div>
            <?php
                }
            ?>

            <a href="tickets.php" class="ui fluid animated simple raised segment button" tabindex="0">
                <div class="visible content">More issues...</div>
                <div class="hidden content">
                    <i class="right arrow icon"></i>
                </div>
            </a>

        </div>

    </div>



    <div class="row"></div>

</div>

<?php
require  ('includes/footer.php');
?>
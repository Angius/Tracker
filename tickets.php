<?php
require ('includes/header.php');
?>

    <div class="ui centered stackable grid main-content">

        <div class="row"></div>

        <div class="relaxed row">

            <div class="eight wide column">
                <?php
                require ('db.php');
                require('includes/get-tickets.php');
                require ('includes/get-users.php');
                require ('includes/get-tags.php');
                require ('includes/count-votes.php');

                require ('includes/truncate.php');

                if (!empty($_GET && $_GET['id'] != "")) {

                    $ticket = getSingleTicket($_GET['id']);
                    $poster = getSingleUser($ticket['ticket_author']);
                    $developer = getSingleUser($ticket['ticket_developer']);
                    $tags = getTagsByTicket($_GET['id']);

                    if (empty($developer)) {
                        $developer['user_id'] = "null";
                        $developer['user_name'] = "<i>NONE</i>";
                        $developer['user_avatar'] = "http://via.placeholder.com/260/ff8800/222222.png?text=NO+AVATAR";
                    }
                    ?>

                    <div class="ui raised segment grid">
                        <div class="row">
                            <div class="twelve wide column">
                                <h2>
                                    <i id="score-btn"></i>
                                    <?=$ticket['ticket_name']?>
                                </h2>
                            </div>
                            <div class="four wide column">
                                <a class="ui right ribbon label" href="categories.php?id=<?=$ticket['category_id']?>">
                                    <?=$ticket['category_name']?>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve wide column">
                                Ticket by
                                <a class="black-link" href="users.php?user=<?= $poster['user_id']?>"><?=$poster['user_name']?></a>
                                <img class="ui avatar image" src="<?=$poster['user_avatar']?>"">
                                Assigned to
                                <a class="black-link" href="users.php?dev=<?= $developer['user_id']?>"><?=$developer['user_name']?></a>
                                <img class="ui avatar image" src="<?=$developer['user_avatar']?>"">
                            </div>
                            <div class="four wide column">
                                <div class="ui right ribbon label">
                                    <?=$ticket['ticket_date']?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ui raised segment padded grid">
                        <div class="row">
                            <?=$ticket['ticket_body']?>
                        </div>
                        <div class="row">
                            <?php
                                foreach ($tags as $tag) {
                                    ?>
                                    <a class="ui tag label" href="tags.php?id=<?= $tag['tag_id'] ?>">
                                        <?= $tag['tag_name'] ?>
                                    </a>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>

                    <br>

                    <div class="ui raised segment">
                        <h3>Comments</h3>
                        <form method="POST" class="ui form" id="comment_form">
                            <div class="fields">
                                <textarea name="body" id="body" class="form-control" placeholder="Enter Comment" rows="5"></textarea>
                                <input type="hidden" name="ticket" id="ticket" value="<?=$_GET['id']?>" />
                                <button type="submit" name="submit" id="submit" class="ui icon button" value="Submit" >
                                    <i class="paper plane icon"></i>
                                </button>
                            </div>
                        </form>

                        <div class="ui comments" id="display_comment"></div>
                    </div>

                    <?php

                }
                else {

                    ?>

                    <div class="ui raised segment">
                        <h2>Recent issues</h2>
                    </div>

                    <?php

                    $tickets = getAllTickets();

                    foreach ($tickets as $ticket) {

                        // Check score
                        $colour = "";
                        if (countVotes($ticket['ticket_id']) < 0)
                            $colour = " red";
                        else if (countVotes($ticket['ticket_id']) > 0)
                            $colour = " green";

                        // Get poster and admin
                        $poster = getSingleUser($ticket['ticket_author']);
                        $developer = getSingleUser($ticket['ticket_developer'])
                        ?>

                        <div class="ui raised segments">
                            <div class="ui grid segment">
                                <div class="row">
                                    <h3 class="twelve wide column">
                                        <div class="ui <?= $colour ?> horizontal circular label">
                                            <i class="heart icon"></i>
                                            <?= countVotes($ticket['ticket_id']); ?>
                                        </div>
                                        <a class="black-link" href="tickets.php?id=<?= $ticket['ticket_id'] ?>"><?= $ticket['ticket_name'] ?></a>
                                    </h3>
                                    <div class="two wide column">
                                        <?= $ticket['ticket_date'] ?>
                                    </div>
                                    <div class="two wide column">
                                        <a class="ui right ribbon label" href="categories.php?id=<?=$ticket['category_id']?>">
                                            <?= $ticket['category_name'] ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="ui segment">
                                <?= truncate($ticket['ticket_body'], 2047) ?>
                            </div>
                        </div>

                        <?php
                    }

                    ?>

                    <a href="tickets.php" class="ui fluid animated simple raised segment button" tabindex="0">
                        <div class="visible content">More issues...</div>
                        <div class="hidden content">
                            <i class="right arrow icon"></i>
                        </div>
                    </a>

                    <?php
                }
                ?>

            </div>

        </div>



        <div class="row"></div>

    </div>

    <script src="js/comments.js"></script>

    <script>
    // Add comment
    $('#comment_form').on('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var data = $("#comment_form").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "includes/comments/comment-add.php",
            success: function(data){
                loadComments();
                $("#body").val('');
            }
        });
        //alert("Err: " + data);
    });
</script>
    <script>
        // Load comment
        function loadComments()
        {
            var id = <?php echo json_encode($_GET['id']); ?>;

            $.ajax({
                type: 'post',
                url: 'includes/comments/comment-list.php',
                data: {
                    id:id
                },
                success: function (response) {
                    // We get the element having id of display_info and put the response inside it
                    $( '#display_comment' ).html(response);
                }
            });
        }
    </script>
    <script>
        // Add vote
        $('#score-btn').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var id = <?php echo json_encode($_GET['id']); ?>;
            $.ajax({
                type: "post",
                url: "includes/votes/vote-add.php",
                data: {
                    id:id
                },
                success: function(data){
                    loadVotes();
                }
            });
            //alert("Err: " + data);
        });
    </script>
    <script>
        // Load votes
        function loadVotes()
        {
            var id = <?php echo json_encode($_GET['id']); ?>;

            $.ajax({
                type: 'post',
                url: 'includes/votes/vote-display.php',
                data: {
                    id:id
                },
                success: function (response) {
                    // We get the element having id of display_info and put the response inside it
                    $( '#score-btn' ).html(response);
                }
            });
        }
    </script>
    <script>
        // On page load
        $(function(){
            loadComments();
            loadVotes();
        });
    </script>


<?php
require  ('includes/footer.php');
?>
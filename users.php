<?php
require ('includes/header.php');
?>

    <div class="ui centered stackable grid main-content">

        <div class="row"></div>

        <div class="relaxed row">

            <div class="eight wide column">
                <?php
                require ('db.php');
                require('includes/get-tickets.php');
                require ('includes/get-users.php');
                require ('includes/get-tags.php');
                require ('includes/get-categories.php');

                require ('includes/truncate.php');

                if (!empty($_GET && $_GET['user'] != "")) {

                    $tickets = getTicketsByUser($_GET['user']);
                    $user = getSingleUser($_GET['user']);

                    ?>
                    <div class="ui raised segment">
                        <h2>Started by <?= $user['user_name'] ?></h2>
                    </div>
                    <?php

                    foreach ($tickets as $ticket) {

                        // Check score
                        $colour = "";
                        if ($ticket['ticket_score'] < 0)
                            $colour = " red";
                        else if ($ticket['ticket_score'] > 0)
                            $colour = " green";
                        ?>

                        <div class="ui raised segments">
                            <div class="ui segment">
                                <a class="black-link" href="tickets.php?id=<?= $ticket['ticket_id'] ?>">
                                    <h3>
                                        <div class="ui <?= $colour ?> horizontal circular label">
                                            <i class="heart icon"></i>
                                            <?= $ticket['ticket_score'] ?>
                                        </div>
                                        <?= $ticket['ticket_name'] ?>
                                    </h3>
                                </a>
                            </div>
                            <div class="ui segment">
                                <?= truncate($ticket['ticket_body'], 255) ?>
                            </div>
                        </div>

                        <?php
                    }

                } else if (!empty($_GET) && $_GET['dev'] != "" && $_GET['dev'] != "null") {

                    $tickets = getTicketsByDev($_GET['dev']);
                    $user = getSingleUser($_GET['dev']);

                    ?>
                    <div class="ui raised segment">
                        <h2>Handled by <?= $user['user_name'] ?></h2>
                    </div>
                    <?php

                    foreach ($tickets as $ticket) {

                        // Check score
                        $colour = "";
                        if ($ticket['ticket_score'] < 0)
                            $colour = " red";
                        else if ($ticket['ticket_score'] > 0)
                            $colour = " green";
                        ?>

                        <div class="ui raised segments">
                            <div class="ui segment">
                                <a class="black-link" href="tickets.php?id=<?= $ticket['ticket_id'] ?>">
                                    <h3>
                                        <div class="ui <?= $colour ?> horizontal circular label">
                                            <i class="heart icon"></i>
                                            <?= $ticket['ticket_score'] ?>
                                        </div>
                                        <?= $ticket['ticket_name'] ?>
                                    </h3>
                                </a>
                            </div>
                            <div class="ui segment">
                                <?= truncate($ticket['ticket_body'], 255) ?>
                            </div>
                        </div>

                        <?php
                    }

                } else if ($_GET['dev'] == "null") {
                    ?>
                    <div class="ui raised segment">
                        <h2>You selected an empty user :(</h2>
                    </div>
                    <?php
                } else {

                    ?>

                    <div class="ui raised segment">
                        <h2>Categories</h2>
                    </div>

                    <div class="ui raised segments">

                        <?php

                        $users = getAllUsers();

                        foreach ($users as $user) {
                            ?>

                            <div class="ui segment">
                                <div class="ui grid">
                                    <div class="one wide column">
                                        <div class="ui label"><?=$user['user_id']?></div>
                                    </div>
                                    <div class="fifteen wide column">
                                        <h2 class="left floated header">
                                            <a href="?id=<?=$user['user_id']?>" class="black-link">
                                                <?=$user['user_name']?>
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }

                        ?>

                    </div>

                    <?php
                }
                ?>

            </div>

        </div>



        <div class="row"></div>

    </div>

<?php
require  ('includes/footer.php');
?>
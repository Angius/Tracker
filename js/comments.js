function post()
{
    var comment = document.getElementById("comment").value;
    var ticket = document.getElementById("ticket").value;

    if(comment && ticket)
    {
        $.ajax
        ({
            type: 'post',
            url: 'comment-add.php',
            data:
                {
                    user_comm:comment,
                    ticket_id:ticket
                },
            success: function (response)
            {
                document.getElementById("all_comments").innerHTML=response+document.getElementById("all_comments").innerHTML;
                document.getElementById("comment").value="";
                document.getElementById("username").value="";

            }
        });
    }

    return false;
}
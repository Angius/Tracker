var mobile = function(){ return jQuery(window).width() <= 768; };
var tablet = function(){ return jQuery(window).width() > 768 && jQuery(window).width() <= 1024; };

function initClasses() {
    if(mobile()){
        $("#fluid").removeClass("ten wide column").addClass("sixteen wide column");
        $("#fluid-thin").removeClass("four wide column").addClass("sixteen wide column");
    } else {
        $("#fluid").removeClass("sixteen wide column").addClass("ten wide column");
        $("#fluid-thin").removeClass("sixteen wide column").addClass("four wide column");
    }
}

jQuery(document).ready(function(){
    initClasses();
    // functions to run when user resizes window
    jQuery(window).resize(function(){
        initClasses();
    });
});
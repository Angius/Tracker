$('#register-form')
    .form({
        fields: {
            login: {
                identifier: 'login',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a username'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter an e-mail'
                    },
                    {
                        type   : 'email',
                        prompt : 'Please enter a correct email address'
                    }
                ]
            },
            email2: {
                identifier: 'email2',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please confirm your e-mail'
                    },
                    {
                        type   : 'match[email]',
                        prompt : 'Emails do not match'
                    }
                ]
            },
            pass: {
                identifier: 'pass',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a password'
                    },
                    {
                        type   : 'minLength[6]',
                        prompt : 'Your password must be at least {ruleValue} characters'
                    }
                ]
            },
            pass2: {
                identifier: 'pass2',
                rules: [
                    {
                        type   : 'match[pass]',
                        prompt : 'Passwords do not match'
                    }
                ]
            },
            terms: {
                identifier: 'terms',
                rules: [
                    {
                        type   : 'checked',
                        prompt : 'You must agree to the terms and conditions'
                    }
                ]
            }


        }
    })
;



$('#login-form')
    .form({
        fields: {
            email: {
                identifier: 'lemail',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your e-mail'
                    }
                ]
            },
            pass: {
                identifier: 'lpass',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    }
                ]
            }
        }
    })
;
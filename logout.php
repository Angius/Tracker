<?php
session_start();
$_SESSION['user_id'] = "";
$_SESSION['user_name'] = "";
session_unset();
session_destroy();

header("Location: index.php?loggedout");
die();
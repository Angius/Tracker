/*******************************
             Docs
*******************************/

/* Paths used for "serve-docs" and "build-docs" tasks */
module.exports = {
  base: '',
  globs: {
    eco: '**/*.html.eco'
  },
  paths: {
    clean: __DIR__ . '/../docs/out/dist/',
    source: {
      config      : 'src/theme.config',
      definitions : 'src/definitions/',
      site        : 'src/site/',
      themes      : 'src/themes/'
    },
    output: {
      examples     : __DIR__ . '/../docs/out/examples/',
      less         : __DIR__ . '/../docs/out/src/',
      metadata     : __DIR__ . '/../docs/out/',
      packaged     : __DIR__ . '/../docs/out/dist/',
      uncompressed : __DIR__ . '/../docs/out/dist/components/',
      compressed   : __DIR__ . '/../docs/out/dist/components/',
      themes       : __DIR__ . '/../docs/out/dist/themes/'
    },
    template: {
      eco: __DIR__ . '/../docs/server/documents/'
    },
  }
};

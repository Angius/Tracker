<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 19.06.2018
 * Time: 03:15
 */

require_once ("../../db.php");

try {
    $sql = "SELECT count(*)
            FROM `votes`
            WHERE `tickets_ticket_id` = :id";

    global $pdo;

    $sth = $pdo->prepare($sql);

    $sth->bindParam(':id', $_POST['id']);

    $sth->execute();
    $votes = $sth->fetchColumn();

    // Check score
    $colour = "";
    if ($votes < 0)
        $colour = " red";
    else if ($votes > 0)
        $colour = " green";

    echo "<div class='ui ". $colour." horizontal circular label'>";
        echo "<i class='heart icon'></i>";
        echo $votes;
    echo "</div>";
}
catch (Exception $e)
{
    var_dump($e);
}

<?php
session_start();

require_once ('../../db.php');

if(isset($_REQUEST))
{
    try {
        $sql = "SELECT count(*)
                FROM `votes`
                WHERE `tickets_ticket_id` = :id AND `users_user_id` = :user";

        $sth = $pdo->prepare($sql);
        $sth->execute(
            array(
                ':id' => $_POST['id'],
                ':user' => $_SESSION['user_id'],
            )
        );
        $any_votes = $sth->fetchColumn();

    }
    catch (Exception $e)
    {
        $data = $e;
    }

    if ($any_votes == 0) {
        try {
            $sql = "INSERT INTO votes 
                 (tickets_ticket_id, users_user_id) 
                 VALUES (:ticket, :user)";
            $sth = $pdo->prepare($sql);
            $sth->execute(
                array(
                    ':ticket' => $_POST['id'],
                    ':user' => $_SESSION['user_id'],
                )
            );
        } catch (Exception $e) {
            $data += $e;
        }
    }
}

echo json_encode($data);

<?php

session_start();

require (__DIR__ . '/../db.php');


if ($_POST['type'] == "login")
{

    // Try to execute the query
    try {
        $sql = "SELECT * FROM users WHERE `user_email` = :email";
        $sth = $pdo->prepare($sql);
        $sth->bindParam(':email', $_POST['lemail']);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
    }
    catch (Exception $e)
    {
        header("Location: ../entry.php?".$e);
        die();
    }

    // Check if the user exists
    if (empty($result) || is_null($result))
    {
        header("Location: ../entry.php?noacc");
        die();
    }
    else
    {
        // If co, check if password is correct
        if (password_verify($_POST['lpass'], $result['user_password']))
        {
            $_SESSION['user_id'] = $result['user_id'];
            $_SESSION['user_name'] = $result['user_name'];

            header("Location: ../index.php?loggedin");
            die();
        }
        else
        {
            header("Location: ../entry.php?wrongcred");
            die();
        }
    }

}
else if ($_POST['type'] == "register")
{
    // Try to get the user
    try {
        $sql = "SELECT * FROM users WHERE `user_name` = :login";
        $sth = $pdo->prepare($sql);
        $sth->bindParam(':login', $_POST['login']);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e)
    {
        header("Location: ../entry.php?".$e);
        die();
    }

    // captcha secret key
    $secret = "6LdDFz4UAAAAANVM40MdeZChzaolOuLX0BRD47Qe";

    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);

    if ($responseData->success) {

        if ($result['user_name'] == $_POST['login']) {
            header("Location: ../entry.php?login-exists");
            die();
        }

        if ($result['user_email'] == $_POST['email']) {
            header("Location: ../entry.php?email-exists");
            die();
        }

// Prepare the password
        $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);

// Prepare the token
        $token = password_hash(($_POST['login'] . $_POST['email'] . time()), PASSWORD_BCRYPT);

// Try to create the user
        try {
            $sql = "INSERT INTO `users` (`user_name`, `user_email`, `user_password`, `user_token`, `user_avatar`, `user_role`)
                    VALUES (:login, :email, :pass, :token, NULL, '5');";
            $sth = $pdo->prepare($sql);
            $sth->bindParam(':login', $_POST['login']);
            $sth->bindParam(':email', $_POST['email']);
            $sth->bindParam(':pass', $pass);
            $sth->bindParam(':token', $token);
            $sth->execute();

        } catch (Exception $e)
        {
            header("Location: ../entry.php?".$e);
            die();
        }




        header("Location: ../index.php");
        die();

    }
    else
    {
        header("Location: ../entry.php?captchaerr");
        die();
    }

}
else
{
    header("Location: ../entry.php?typeerr");
}
<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 19.06.2018
 * Time: 03:15
 */

require_once (__DIR__."/../db.php");

function countVotes($id) {
    $votes = "error";

    try {
        $sql = "SELECT count(*)
                FROM `votes`
                WHERE `tickets_ticket_id` = :id";

        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $votes = $sth->fetchColumn();

    } catch (Exception $e) {
        var_dump($e);
    }

    return $votes;
}

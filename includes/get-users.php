<?php
require (__DIR__ . '/../db.php');

// Get all news from database
function getAllUsers() {
    $users = [];

    try {
        $sql = "SELECT `users`.`user_id`, `users`.`user_name`, `users`.`user_email`, `users`.`user_token`, `users`.`user_avatar`, `roles`.* 
                FROM `users` 
                LEFT JOIN `roles` ON `users`.`user_role` = `roles`.`role_id` ";

        global $pdo;

        $sth = $pdo->prepare($sql);
        $sth->execute();
        $users = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $users;
}

// Get a single ticket
function getSingleUser($id) {
    $user = [];

    try {
        $sql = "SELECT `users`.`user_id`, `users`.`user_name`, `users`.`user_email`, `users`.`user_token`, `users`.`user_avatar`, `roles`.* 
                FROM `users` 
                LEFT JOIN `roles` ON `users`.`user_role` = `roles`.`role_id`
                WHERE `user_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $user = $sth->fetch();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $user;
}

<?php
session_start();
//add_comment.php

require_once ('../../db.php');

if(isset($_REQUEST))
{
    try {
        $sql = "INSERT INTO comments 
             (comment_ticket, comment_date, comment_author, comment_body, comment_score) 
             VALUES (:ticket, :date, :author, :body, 0)";
        $sth = $pdo->prepare($sql);
        $sth->execute(
            array(
                ':ticket' => $_POST['ticket'],
                ':date' => date("Y-m-d H:i:s"),
                ':author' => $_SESSION['user_id'],
                ':body' => $_POST['body'],
            )
        );
    } catch (Exception $e){
        $data = $e;
    }
}

echo json_encode($data);

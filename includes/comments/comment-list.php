<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 19.06.2018
 * Time: 03:15
 */

require_once ("../../db.php");

try {
    $sql = "SELECT * FROM `comments`
            JOIN `users` ON `users`.`user_id` = `comments`.`comment_author`
            WHERE comment_ticket = :id
            ORDER BY comment_date DESC";

    global $pdo;

    $sth = $pdo->prepare($sql);

    $sth->bindParam(':id', $_POST['id']);

    $sth->execute();
    $comments = $sth->fetchAll();

    foreach ($comments as $comment) {

        echo "<div class='ui comment'>";
            echo "<a class='avatar'>";
                echo "<img src=".$comment['user_avatar'].">";
            echo "<a>";
            echo "<div class='content'>";
                echo "<a class='author'>" .$comment['user_name']."</a>";
                echo "<div class='metadata'>";
                    echo "<div class='date'>".$comment['comment_date']."</div>";
                echo "</div>";
                echo "<div class='text'>".$comment['comment_body']."</div>";
            echo "</div>";
        echo "</div>";

    }
}
catch (Exception $e)
{
    var_dump($e);
}

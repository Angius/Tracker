<?php session_start() ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tracker</title>

    <!-- SemanticUI -->
    <link rel="stylesheet" type="text/css" href="../semantic/dist/semantic.min.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="../semantic/dist/semantic.min.js"></script>

    <!-- AJAX -->


    <link rel="stylesheet" type="text/css" href="../css/styles.css">

    <!-- Includes -->
    <? require (__DIR__ . '/../db.php') ;

    ?>
</head>

<?php
// Check if user logged in
$userlink = "";
$logoutlink = "";

if (array_key_exists('user_id',$_SESSION) && !empty($_SESSION['user_id'])) {
    $userlink = '<a href="../user.php" class="item"><i class="ui user circle icon"></i>Profile</a>';
    $logoutlink = '<a href="../logout.php" class="item"><i class="ui power off icon"></i>Log out</a>';
} else {
    $userlink = '<a href="../entry.php" class="item"><i class="ui user circle icon"></i>Login/register</a>';
    $logoutlink = "";
}

?>

<body>
<div class="ui simple toppest floating menu">
    <a href="../index.php" class="item">
        <img src="../media/logo.svg">
    </a>

    <div class="ui right dropdown item">
        <i class="ui bars icon"></i>
        <i class="dropdown icon"></i>
        <div class="menu">
            <?=$userlink?>
            <a href="../entry.php" class="item">
                <i class="ui info circle icon"></i>
                About
            </a>
            <a href="../entry.php" class="item">
                <i class="ui envelope icon"></i>
                Contact
            </a>
            <?=$logoutlink?>
        </div>
    </div>

</div>
<?php
require (__DIR__ . '/../db.php');

// Get all tags from database
function getAllTags() {
    $tags = [];

    try {
        $sql = "SELECT *
                FROM `tags` ";

        global $pdo;

        $sth = $pdo->prepare($sql);
        $sth->execute();
        $tags = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tags;
}

// Get a single tag
function getSingleTag($id) {
    $tag = [];

    try {
        $sql = "SELECT *
                FROM `tags`
                WHERE `tag_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $tag = $sth->fetch();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tag;
}

// Get tags by article
function getTagsByTicket($ticket_id) {
    $tags = [];

    try {
        $sql = "SELECT `tags`.*, `tickets_has_tags`.*
                FROM `tags`
                JOIN `tickets_has_tags` ON `tags`.`tag_id` = `tickets_has_tags`.`tags_tag_id`
                WHERE `tickets_ticket_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $ticket_id);

        $sth->execute();
        $tags = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tags;
}

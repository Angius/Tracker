<?php
require (__DIR__ . '/../db.php');

// Get all news from database
function getAllTickets() {
    $tickets = [];

    try {
        $sql = "SELECT `tickets`.*, `categories`.*
                FROM `tickets`
                LEFT JOIN `categories` ON `tickets`.`ticket_category` = `categories`.`category_id`
                ORDER BY `ticket_date` DESC";
        global $pdo;

        $sth = $pdo->prepare($sql);
        $sth->execute();
        $tickets = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tickets;
}

// Get a single ticket
function getSingleTicket($id) {
    $ticket = [];

    try {
        $sql = "SELECT `tickets`.*, `categories`.*
            FROM `tickets`
                LEFT JOIN `categories` ON `tickets`.`ticket_category` = `categories`.`category_id`
            WHERE `ticket_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $ticket = $sth->fetch();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $ticket;
}

// Get a single ticket by cat
function getTicketsByCategoryId($id) {
    $tickets = [];

    try {
        $sql = "SELECT `tickets`.*, `categories`.*
                FROM `tickets`
                LEFT JOIN `categories` ON `tickets`.`ticket_category` = `categories`.`category_id`
                WHERE `ticket_category` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $tickets = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tickets;
}

// Get tickets by tag ID
function getTicketsByTag($tag_id) {
    $tickets = [];

    try {
        $sql = "SELECT `tickets`.*, `tickets_has_tags`.*
                FROM `tickets`
                JOIN `tickets_has_tags` ON `tickets`.`ticket_id` = `tickets_has_tags`.`tickets_ticket_id`
                WHERE `tags_tag_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $tag_id);

        $sth->execute();
        $tickets = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tickets;
}

// Get tickets by tag ID
function getTicketsByUser($user_id) {
    $tickets = [];

    try {
        $sql = "SELECT *
                FROM `tickets`
                WHERE `ticket_author` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $user_id);

        $sth->execute();
        $tickets = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tickets;
}

// Get tickets by tag ID
function getTicketsByDev($dev_id) {
    $tickets = [];

    try {
        $sql = "SELECT *
                FROM `tickets`
                WHERE `ticket_developer` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $dev_id);

        $sth->execute();
        $tickets = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $tickets;
}
<?php

require (__DIR__ . '/../db.php');

// Get all news from database
function getAllNews() {
    $news = [];

    try {
        $sql = "SELECT * FROM `news`";
        global $pdo;

        $sth = $pdo->prepare($sql);
        $sth->execute();
        $news = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $news;
}

// Get a news my ID from database
function getSingleNews($id) {
    $news = [];

    try {
        $sql = "SELECT * FROM `news` WHERE `news_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $news = $sth->fetch();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $news;
}
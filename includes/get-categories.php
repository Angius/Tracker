<?php
require (__DIR__ . '/../db.php');

// Get all categories from database
function getAllCategories() {
    $categories = [];

    try {
        $sql = "SELECT *
                FROM `categories`";
        global $pdo;

        $sth = $pdo->prepare($sql);
        $sth->execute();
        $categories = $sth->fetchAll();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $categories;
}

// Get a single category
function getSingleCategory($id) {
    $category = [];

    try {
        $sql = "SELECT *
                FROM `categories`
                WHERE `category_id` = :id";
        global $pdo;

        $sth = $pdo->prepare($sql);

        $sth->bindParam(':id', $id);

        $sth->execute();
        $category = $sth->fetch();
    }
    catch (Exception $e)
    {
        var_dump($e);
    }

    return $category;
}

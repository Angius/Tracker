<?php
require ('includes/header.php');
?>

    <div class="ui centered stackable grid main-content">

        <div class="row"></div>

        <div class="relaxed row">

            <div class="eight wide column">
                <?php
                require ('db.php');
                require ('includes/get-news.php');
                require ('includes/truncate.php');

                if (empty($_GET) || $_GET['id'] == '') {
                    $news = getAllNews();

                    foreach ($news as $n) {
                        ?>
                        <div class="ui raised segment">
                            <h2>News</h2>
                        </div>

                        <div class="ui raised segments">
                            <div class="ui segment">
                                <a class="black-link" href="news.php?id=<?= $n['news_id'] ?>">
                                    <h3>
                                        <div class="ui horizontal circular label">
                                            <?= date("d.m.Y", strtotime($n['news_date'])) ?>
                                        </div>
                                        <?= $n['news_title'] ?>
                                    </h3>
                                </a>
                            </div>
                            <div class="ui segment">
                                <?= truncate($n['news_body'], $n['news_truncate']) ?>
                                <a href="news.php?id=<?= $n['news_id'] ?>"> Read more</a>
                            </div>
                        </div>

                        <a href="news.php" class="ui fluid animated simple raised segment button" tabindex="0">
                            <div class="visible content">Older news...</div>
                            <div class="hidden content">
                                <i class="right arrow icon"></i>
                            </div>
                        </a>
                        <?php
                    }
                }
                else
                {
                    $news = getSingleNews($_GET['id']);

                    ?>
                    <div class="ui raised segment">
                        <h2><?= $news['news_title'] ?></h2>
                    </div>

                    <div class="ui raised segments">
                        <div class="ui segment">
                            <div class="ui horizontal circular label">
                                <?= date("d.m.Y", strtotime($news['news_date'])) ?>
                            </div>
                        </div>
                        <div class="ui segment">
                            <?= $news['news_body'] ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

        </div>



        <div class="row"></div>

    </div>

<?php
require  ('includes/footer.php');
?>
<?php
if (empty($_POST) || $_POST == []) {
    header("Location: ../index.php");
    die();
}

file_put_contents('errors.txt', "BEGIN:: ", FILE_APPEND);

require (__DIR__ . '/../db.php');

// Try to execute the query
try {
    $sql = "SELECT * FROM users WHERE `user_name` = :login";
    $sth = $pdo->prepare($sql);
    $sth->bindParam(':login', $_POST['login']);
    $sth->execute();
    $result = $sth->fetch(PDO::FETCH_ASSOC);
}
catch (Exception $e)
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('errors.txt', "1: " . $e . "\r\n", FILE_APPEND);
}

// Check if the user exists
if (empty($result) || is_null($result))
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('errors.txt', "2: " . $result . "\r\n", FILE_APPEND);
}
else
{
    // If so, check if password is correct
    if (password_verify($_POST['pass'], $result['user_password']))
    {
        // Prepare the token
        $token = password_hash(($result['user_name'] . $result['user_email'] . time()), PASSWORD_BCRYPT);

        // Try to add the token
        try {
            $sql = "UPDATE `users` SET `user_token` = :token
                    WHERE `users`.`user_name` = :login";
            $sth = $pdo->prepare($sql);
            $sth->bindParam(':token', $token);
            $sth->bindParam(':login', $_POST['login']);
            $sth->execute();

        } catch (Exception $e)
        {
            header('X-PHP-Response-Code: 401', true, 401);
            file_put_contents('errors.txt', "3: " . $e . "\r\n", FILE_APPEND);
        }

        // Construct Json
        $result_json = array('token' => $token);

        // headers to tell that result is JSON
        header('Content-type: application/json');

        // send the result now
        echo json_encode($result_json);

        header('X-PHP-Response-Code: 201', true, 201);
        file_put_contents('errors.txt', "SUCC: ", FILE_APPEND);
        foreach ($result_json as $j)
            file_put_contents('errors.txt', "j: " . $j . "\r\n", FILE_APPEND);
    }
    else
    {
        header('X-PHP-Response-Code: 401', true, 401);
        file_put_contents('errors.txt', "4: " . $_POST['pass'] . $result['user_password'] . "\r\n", FILE_APPEND);
    }
}

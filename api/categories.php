<?php

file_put_contents('cat-errors.txt', "BEGIN:: " . "\r\n", FILE_APPEND);

require (__DIR__ . '/../db.php');

// Try to get categories
try {
    $sql = "SELECT * FROM categories";
    $sth = $pdo->prepare($sql);
    $sth->execute();
    $categories = $sth->fetchAll();
}
catch (Exception $e)
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('cat-errors.txt', "2: " . $e . "\r\n", FILE_APPEND);
    die();
}

// Construct Json
$result_json = array();
foreach ($categories as $cat) {
    $result_json[$cat['category_id']] = $cat['category_name'];
    file_put_contents('cat-errors.txt', "    >" . $cat['category_name'] . "\r\n", FILE_APPEND);
}

// headers to tell that result is JSON
header('Content-type: application/json');

// send the result now
echo json_encode($result_json);

file_put_contents('errors.txt', "SUCC: ", FILE_APPEND);
header('X-PHP-Response-Code: 201', true, 201);
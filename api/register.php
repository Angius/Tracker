<?php
if (empty($_POST) || $_POST == []) {
    header("Location: ../index.php");
    die();
}

require (__DIR__ . '/../db.php');

// Prepare the password
$pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);

// Prepare the token
$token = password_hash(($_POST['login'] . $_POST['email'] . time()), PASSWORD_BCRYPT);

// Try to create the user
try {
    $sql = "INSERT INTO `users` (`user_name`, `user_email`, `user_password`, `user_token`, `user_avatar`, `user_role`)
                    VALUES (:login, :email, :pass, :token, NULL, '5');";
    $sth = $pdo->prepare($sql);
    $sth->bindParam(':login', $_POST['login']);
    $sth->bindParam(':email', $_POST['email']);
    $sth->bindParam(':pass', $pass);
    $sth->bindParam(':token', $token);
    $sth->execute();

} catch (Exception $e)
{
    header('X-PHP-Response-Code: 400', true, 400);
}

header('X-PHP-Response-Code: 200', true, 200);
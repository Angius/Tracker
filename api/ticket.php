<?php
if (empty($_POST) || $_POST == []) {
    file_put_contents('ticket-errors.txt', "FAILURE:: " . "\r\n", FILE_APPEND);
    header("Location: ../index.php");
    die();
}

file_put_contents('ticket-errors.txt', "BEGIN:: " . "\r\n", FILE_APPEND);

require (__DIR__ . '/../db.php');

// Try to get user by token
try {
    $sql = "SELECT * FROM users WHERE `user_token` = :token";
    $sth = $pdo->prepare($sql);
    $sth->bindParam(':token', $_POST['token']);
    $sth->execute();
    $user = $sth->fetch(PDO::FETCH_ASSOC);
}
catch (Exception $e)
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('ticket-errors.txt', "1: " . $e . "\r\n", FILE_APPEND);
}

// Try to get category by name
try {
    $sql = "SELECT * FROM categories WHERE `category_name` = :name";
    $sth = $pdo->prepare($sql);
    $sth->bindParam(':name', $_POST['cat']);
    $sth->execute();
    $category = $sth->fetch(PDO::FETCH_ASSOC);
}
catch (Exception $e)
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('ticket-errors.txt', "1: " . $e . "\r\n", FILE_APPEND);
}

// Check if the user exists
if (empty($user) || is_null($user))
{
    header('X-PHP-Response-Code: 401', true, 401);
    file_put_contents('ticket-errors.txt', "2: " . $user . "\r\n", FILE_APPEND);
}
else
{
    // If so, check if password is correct
    if ($user['user_token'] == $_POST['token'])
    {
        // Try to add the token
        try {
            $sql = "INSERT INTO `tickets` (`ticket_id`, `ticket_name`, `ticket_body`, `ticket_date`, `ticket_score`, `ticket_category`, `ticket_severity`, `ticket_author`, `ticket_developer`)
                    VALUES (NULL, :title, :body, :datetime, '0', :category, '6', :author, NULL);";
            $sth = $pdo->prepare($sql);
            $sth->bindParam(':title', $_POST['title']);
            $sth->bindParam(':body', $_POST['body']);
            $sth->bindParam(':datetime', gmdate('Y-m-d H:i:s'));
            $sth->bindParam(':category', $category['category_id']);
            $sth->bindParam(':author', $user['user_id']);
            $sth->execute();

        } catch (Exception $e)
        {
            header('X-PHP-Response-Code: 401', true, 401);
            file_put_contents('ticket-errors.txt', "3: " . $e . "\r\n", FILE_APPEND);
        }

        // Construct Json
        $result_json = array('url' => $_SERVER['HTTP_HOST'] . "/ticket.php?id=" . str_replace(" ","-", $_POST['title']));

        // headers to tell that result is JSON
        header('Content-type: application/json');

        // send the result now
        echo json_encode($result_json);

        header('X-PHP-Response-Code: 201', true, 201);
        file_put_contents('errors.txt', "SUCC: ", FILE_APPEND);
        foreach ($result_json as $j)
            file_put_contents('ticket-errors.txt', "j: " . $j . "\r\n", FILE_APPEND);
    }
    else
    {
        header('X-PHP-Response-Code: 401', true, 401);
        file_put_contents('ticket-errors.txt', "4: " . $_POST['token'] . " should be " . $user['user_token'] . "\r\n", FILE_APPEND);
    }
}